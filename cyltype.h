//
// Created by Christopher Hurst on 11/20/15.
//

#ifndef CYLTYPE_H
#define CYLTYPE_H

#include <iostream>
#include <cmath>
#include "circletype.h"

using namespace std;

class cyltype : public circletype{

private:
    double height;
public:
    cyltype();
    cyltype(double, double);
    void setheight(double);
    double getheight() const;
    double volume() const;
    double surfacearea() const;
    void print() const;
};
cyltype::cyltype(){
    height = 1;
}
cyltype::cyltype(double r, double h)
        :circletype(r){
    height = h;
}
void cyltype::setheight(double h){
    height = h;
}
double cyltype::getheight() const{
    return height;
}
double cyltype::volume() const{
    return (M_PI * pow(circletype::area() ,2) * height);
}
double cyltype::surfacearea() const{
    return (2*M_PI*circletype::getrad()*height);
}
void cyltype::print() const{
    cout << "Height = " << getheight() << endl;
    cout << "Volume = " << volume() << endl;
    cout << "Surface Area = " << surfacearea() << endl;

}


#endif //CYLTYPE_H
