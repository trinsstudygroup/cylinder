/*
	Name: Chris Hurst
	Class: CSC160
	Due Date: 11/20/15
	Files: cylinder.cpp, circletype.h, cyltype.h
	Description:
 */

#include <iostream>
#include <iomanip>

#include <curses.h>
#include "cyltype.h"



using namespace std;

int main() {

    int r, theta, h;

    circletype mycir;
    cyltype mycyl;

    cout << fixed << showpoint << setprecision(2);

    cout << "What is the radius of your circle: ";
    while(!(cin >> r) || r <= 0){
        if ( !cin )
        {
            cout << "ERROR: You have entered an invalid character, please enter a number: ";
            cin.clear();
            cin.ignore(200,'\n');
        }
        else
            cout << "ERROR: The number must be greater than zero, please reenter: ";
    }
    mycir.setrad(r);
    getch();

    cout << "Radius = " << mycir.getrad() << endl;

    cout << "Enter the degrees and ill tell you the arc length: ";
    while(!(cin >> theta) || theta <= 0){
        if ( !cin )
        {
            cout << "ERROR: You have entered an invalid character, please enter a number: ";
            cin.clear();
            cin.ignore(200,'\n');
        }
        else
            cout << "ERROR: The number must be greater than zero, please reenter: ";
    }
    cout << "Area = " << mycir.area() << endl;
    cout << "Circumfernce = " << mycir.circum() << endl;
    cout << "Arc = " << mycir.arc(theta) << endl;
    cout << "Diameter = " << mycir.dia() << endl;
    cout << "*****************************" << endl;

    mycir.print(theta);

    cout << "*****************************" << endl;
    cout << "Info for my cylinder" << endl;
    cout << "What is the height of your cylinder: ";
    while(!(cin >> h) || h <= 0){
        if ( !cin )
        {
            cout << "ERROR: You have entered an invalid character, please enter a number: " ;
            cin.clear();
            cin.ignore(200,'\n');
        }
        else
            cout << "ERROR: The number must be greater than zero, please reenter: " ;
    }
    mycyl.setheight(h);
    cout << "height = " << mycyl.getheight() << endl;
    cout << "Volume = " << mycyl.volume() << endl;
    cout << "Serface Area = " << mycyl.surfacearea() << endl;

    cout << "*****************************" << endl;

    mycyl.print();
    return 0;
}