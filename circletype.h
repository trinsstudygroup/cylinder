//
// Created by Christopher Hurst on 11/20/15.
//

#ifndef CRICLETYPE_H
#define CRICLETYPE_H

#include <iostream>
#include <cmath>


using namespace std;

class circletype{
private:

    double radius;

public:

    circletype();
    circletype(double);
    void setrad(double);
    double getrad() const;
    double area() const;
    double circum() const;
    double arc(double) const;
    double dia() const;
    void print (double) const;
};

circletype::circletype(){

    radius = 1;
}
circletype::circletype(double r){ // default constructor
    radius = r;
}
void circletype::setrad(double r){
    radius = r;
}
double circletype::getrad() const{
    return radius;
}
double circletype::area() const{
    return (M_PI * pow(radius,2));
}
double circletype::circum() const{
    return (2 * M_PI * radius);
}
double circletype::arc(double theta) const{
    double arc, radians;
    radians = theta * (M_PI/180);
    arc = radius * radians;
    return arc;
}
double circletype::dia() const{
    return (radius * 2);
}
void circletype::print (double theta) const{
    cout << "Info for your circle" << endl;
    cout << "Radius = " << getrad() << endl;
    cout << "Area = " << area() << endl;
    cout << "Circumfernce = " << circum() << endl;
    cout << "Arc = " << arc(theta) << endl;
    cout << "Diameter = " << dia() << endl;
}

#endif //CRICLETYPE_H
